import {Component} from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import * as firebase from 'firebase';

//Ce composant permet de lister les personnes ainsi que d'ajouter des pdf
@Component({
  selector: 'lister-personnes',
  templateUrl: './lister-personnes.component.html',
})
export class ListerPersonnesComponent {

	users: FirebaseListObservable<any>;//liste d'utilisateurs
	pdf: Array<string>;//tableau d'adresses de pdf

	constructor(public af: AngularFire){
		this.users = af.database.list('/users');
	}

	showPdf(user: any){//renvoie le tableau des adresses des pdf
		this.pdf = [];//Le tableau de pdf est vidé
		for (var i = 0 ; i < (user.fichier).length; i++) {
			var storageRef = firebase.storage().ref().child(user.fichier[i]);//prend le nom du fichier
			storageRef.getDownloadURL().then(url => this.pdf.push(url));//on push les urls dans le tableau
		}
	}

	ajouterPdf(user: any, fichier: any){//Si l'on veut rajouter un pdf
		var key = user.$key;

		var storageRef = firebase.storage().ref();//ref vers le storage

	    var selectedFile = (<HTMLInputElement>document.getElementById('fichier')).files[0];
	    console.log(selectedFile.name);
    	var path = `${selectedFile.name}`;//chemin dans le storage vers le fichier
    	var iRef = storageRef.child(path);//la ref pointe vers le 'path'
    	iRef.put(selectedFile);//on fait pointer la ref vers le fichier (dans le storage)

    	var indice = (user.fichier).length;//indice du nouvel élément
    	console.log('erere');
    	this.users = this.af.database.list('/users/' + key );//on pointe vers notre chemin
		this.users.update(key, { indice : selectedFile.name});//On push le nom du nouveau pdf dans la database
		this.users = this.af.database.list('/users');//On remet le path sur les users
	}
}

