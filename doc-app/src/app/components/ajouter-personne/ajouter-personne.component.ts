import {Component} from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import * as firebase from 'firebase';

//Ce composant permet d'ajouter une personne(nom, prénom) et son fichier
@Component({
  selector: 'ajouter-personne',
  templateUrl: './ajouter-personne.component.html',
})
export class AjouterPersonneComponent {

	nom: any;
	prenom: any;
	fichier: any;

	boo: boolean;//Pour afficher Utilisateur ajouté

	user: FirebaseListObservable<any>;

	constructor(public af: AngularFire) {
		this.user = af.database.list('/users');//les utilisateurs sont placés dans /users
	}

	onAddSubmit(){
		var nouvelle_entree = {
			nom: this.nom,
			prenom: this.prenom,
			fichier: []
		}
		
	    var storageRef = firebase.storage().ref();//ref vers le storage

	    var selectedFile = (<HTMLInputElement>document.getElementById('fichier')).files[0];
    	var path = `${selectedFile.name}`;//chemin de la bdd vers le fichier
    	var iRef = storageRef.child(path);//la ref pointe vers le 'path'
    	(nouvelle_entree.fichier).push(selectedFile.name);//on push le nom du fichier dans notre tableau
    	iRef.put(selectedFile);//on fait pointer la ref vers le fichier (dans le storage)
      	this.user.push(nouvelle_entree);//on push le tout
	     
	    this.boo=true;
	}
}
