import { Routes } from '@angular/router';

import { AjouterPersonneComponent } from './components/ajouter-personne/ajouter-personne.component';
import { HomeComponent } from './components/home/home.component';
import { ListerPersonnesComponent } from './components/lister-personnes/lister-personnes.component';

//Liste des routes
export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'ajouter-personne', component: AjouterPersonneComponent},
  { path: 'lister-personnes', component: ListerPersonnesComponent }
];

