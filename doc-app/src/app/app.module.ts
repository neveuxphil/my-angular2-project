import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';//module des routes
import { rootRouterConfig } from './app.routes';//module de la config des routes
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule, AuthProviders,AuthMethods } from 'angularfire2';

import { AppComponent } from './app.component';
import { AjouterPersonneComponent } from './components/ajouter-personne/ajouter-personne.component';
import { HomeComponent } from './components/home/home.component';
import { ListerPersonnesComponent } from './components/lister-personnes/lister-personnes.component';

// Initialize Firebase
export const firebaseConfig = {
    apiKey: 'AIzaSyBzrmyHuBiWOsiOzRVNUxJEDv5geYe6wDA',
    authDomain: 'projet-test-8d87e.firebaseapp.com',
    databaseURL: 'https://projet-test-8d87e.firebaseio.com',
    storageBucket: 'projet-test-8d87e.appspot.com',
    messagingSenderId: '219940670652'
};

@NgModule({
  declarations: [
    AppComponent,
    AjouterPersonneComponent,
    HomeComponent,
    ListerPersonnesComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    FormsModule,
    HttpModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
